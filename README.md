# Niessner-ReactNative-Template

Simple React-Native template with following packages pre-installed:

+ @react-navigation/native
+ @react-navigation/stack
+ i18n-js
+ native-base
+ prop-types
+ react-native-localize
+ redux
+ react-redux
+ redux-thunk 
