import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {translate} from '../../util/translator';

const Stack = createStackNavigator();


const MainView = (props) => {
    return (
        <View>
            <Text>{translate("hello-world")} Template</Text>
        </View>
    );
};

const Main = (props) => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Main" component={MainView}/>
        </Stack.Navigator>
    );
};

const styles = StyleSheet.create({});

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});


export default connect(mapStateToProps, mapDispatchToProps)(Main);
