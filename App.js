import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {store} from './src/store';
import {StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import Main from './src/components/pages/Main';
import * as RNLocalize from 'react-native-localize';
import {setI18nConfig} from './src/util/translator';
import {Root, StyleProvider} from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';


class App extends Component {


    constructor(props) {
        super(props);
        console.disableYellowBox = true;
        setI18nConfig(); // set initial config
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }

    handleLocalizationChange = () => {
        setI18nConfig();
        this.forceUpdate();
    };

    render() {
        return (
            <Provider store={store}>
                <StyleProvider style={getTheme(platform)}>
                    <Root>
                        <NavigationContainer>
                            <Main/>
                        </NavigationContainer>
                    </Root>
                </StyleProvider>
            </Provider>
        );
    }
};

const styles = StyleSheet.create({});


export default App;
